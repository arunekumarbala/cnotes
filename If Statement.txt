C – If statement
Submitted by,
NIRANJAN A, MCA PU , 2020

Decision making is crucial for computer programming and real world applications require a lot of decision making.
Although computers maybe highly powerful computational devices, it has no intelligence to make decisions on its own.
It needs help in order to make decisions. This is where selection statements come into play.
Essentially, selection statements give the decision making ability to computer.
Among selection statements, the most widely used is the “if statement”.

The basic outline/syntax of if statement in C looks something like this:

Simple If-statement:

if(condition){
	statement;
}

If - else statement:
if(condition){
	statement1;
}
else{
	statement2;
}


Working:
Simple - If statement
The compiler first checks the condition and 
Case 1: If condition is true, the code block (i.e. statements) inside the if block gets executed.
Case 2: If condition is false, the code block (i.e. statements) inside the if block gets ignored and the compiler moves on.

If - else statement:
This has the exact same working as a simple If statement.
But, in addition to the if statement, there’s an else statement that gets executed when the if-condition is false.



Sample program:
#include <stdio.h>
int main(){
    int num;
    printf("Enter an integer: ");
    scanf("%d", &num);
    if(num%2==0){
        printf("Input is even");
    }
    else{
        printf("Input is odd");
    }
    return 0;
}

Input & Output
Trial 1: 
Enter an integer: 28
Input is even
Trial :2
Enter an integer: 31
Input is odd

Multiple conditions with if statement:
If statements work best when a decision is to be taken based on a single condition.
But there are real-life situations when a decision needs to be taken based on multiple conditions.
In order to cater to this need, we can specify multiple conditions using logical operators. 
In case there are too many conditions, switch-case statement would come in handy.



Logical Operator	Operator Name	Working							Example
&&			Logical AND	Output is true only if both the inputs are true.	if(n%2==0 && n%3==0)
												This is divisibility condition for 6

||			Logical OR	Output is true only if any of the inputs is true.	if(grade == ‘A’ || grade ==’B’)
												This is condition for pass

!			Logical NOT	Output is true if input is false and vice-versa		if(!(grade== ‘F’))
												This is another condition for pass
 
