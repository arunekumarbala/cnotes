by
Adhithya Harshan

Nested Structures

Structures within a structure

If the data type of a member of a structure is a structure of a different type, it is called a nested structure.


Declaration:

struct struct1
{
    datatype1 member1
    datatype2 member2
};

struct struct2
{
	struct struct1 x
	datatype member
} var1;

var1.x.member1


For example :

struct address
{
    int streetno;
    char city[10];
};

struct employee
{
    int emp_id;
    char name[20];
    struct address ad; 
} emp1;

main()
{
    emp1.ad.streetno=44;
    emp1.ad.city="Pondicherry";
    ...
}
